import asyncio
import unittest
import functools
import logging
import os
import os.path
import shutil

from watchdog.watchdog import WatchDog, CondorLogWatcher

log = logging.getLogger(__name__)


def setUpModule():
    os.mkdir("tmp")


def tearDownModule():
    shutil.rmtree("tmp")


def _create(n, name):
    log.debug("Create %s", name)
    with open(name, 'w') as f:
        f.write('baz')


class TestWatchdog(unittest.TestCase):
    # Empty the tmp dir
    def tearDown(self):
        for file in os.listdir("tmp"):
            path = os.path.join("tmp", file)
            log.debug("Remove %s", path)
            if os.path.isfile(path):
                os.remove(path)
            elif os.path.isdir(path):
                shutil.rmtree(path)
            else:
                raise

    # Create WatchDog for non-existing file
    # Then create file and see if handler is invoked
    def test_creation(self):
        log.info("Start test_creation")

        async def foo():
            await asyncio.sleep(0.2)
            _create("", "tmp/foo")
            await asyncio.sleep(0.1)

        async def main():
            wd = WatchDog(
                'tmp/foo',
                handler=functools.partial(_create, name="tmp/bar")
            )
            t = asyncio.wait_for(wd.run(), timeout=0.4)
            await asyncio.gather(t, foo(), return_exceptions=False)

        self.assertFalse(os.path.exists("tmp/bar"))
        try:
            asyncio.run(main())
        except asyncio.exceptions.TimeoutError:
            pass
        self.assertTrue(os.path.exists("tmp/bar"))

    # Create WatchDog for existing file
    # Then modify file and see if the _act method is invoked
    def test_modify(self):
        log.info("Start test_modify")

        class WD(WatchDog):
            def _act(self, registration=False):
                _create("", "tmp/bar")

        _create("", "tmp/foo")

        async def foo():
            await asyncio.sleep(0.2)
            _create("", "tmp/foo")
            await asyncio.sleep(0.1)

        async def main():
            wd = WD('tmp/foo')
            t = asyncio.wait_for(wd.run(), timeout=0.4)
            await asyncio.gather(t, foo(), return_exceptions=False)

        self.assertFalse(os.path.exists("tmp/bar"))
        try:
            asyncio.run(main())
        except asyncio.exceptions.TimeoutError:
            pass
        self.assertTrue(os.path.exists("tmp/bar"))


class TestCondorLogWatcher(unittest.TestCase):
    def test_parsing(self):
        # We need an existing loop before creating the queue
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)

        queue = asyncio.Queue(5)
        wd = CondorLogWatcher('tests/history_', queue=queue)

        # We need to catch the exception inside of asyncio.run so that
        # the loop will close
        async def run():
            try:
                await asyncio.wait_for(wd.run(), 0.1)
            except asyncio.TimeoutError:
                pass

        asyncio.run(run())
        job = asyncio.run(queue.get())
        queue.task_done()
        self.assertEqual(
            job['id'],
            'cloud-htcondor-ce-1-kit.gridka.de#2208666.0#1669649360'
        )
        self.assertEqual(job['droneid'], 'wuppertal-e69f338bbb')
        self.assertTrue(job['harvester_workerid'] is None)
        self.assertEqual(job['state'], "Finished")
        job = asyncio.run(queue.get())
        queue.task_done()
        self.assertEqual(
            job['id'],
            'cloud-htcondor-ce-1-kit.gridka.de#2208674.0#1669649498'
        )
        self.assertTrue(queue.empty())





