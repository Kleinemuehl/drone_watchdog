import asyncio
import logging
import socket
import unittest
import threading

from watchdog.sender import Sender, to_influx

log = logging.getLogger(__name__)


class TestSender(unittest.TestCase):
    def test_tcp(self):
        # Prepare messages
        queue = asyncio.Queue(5)
        msgs = [{"a": 1, "b": 2}, {"a": 1, "b": 3}]
        for m in msgs:
            queue.put_nowait(m)

        # Open socket to receive
        rcv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        rcv.bind(('127.0.0.1', 8099))
        rcv.listen(5)

        def recv_and_check(m):
            conn, addr = rcv.accept()
            r = conn.recv(4096).decode()
            m = to_influx("jobs", m, tags={"FOO": "bar"}).rsplit(maxsplit=1)[0]
            self.assertTrue(r.find(m) == 0)
            conn.close()

        snd = Sender(
            queue,
            host='127.0.0.1',
            port='8099',
            tcp=True,
            tags={"FOO": "bar"}
        )
        asyncio.run(snd.send())
        asyncio.run(snd.send())
        for m in msgs:
            recv_and_check(m)

        rcv.close()

    def test_udp(self):
        # We need an existing loop before creating the queue
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        # Prepare messages
        queue = asyncio.Queue(5)
        msgs = [{"a": 1, "b": 2}, {"a": 1, "b": 3}]
        for m in msgs:
            queue.put_nowait(m)

        # Open socket to receive
        rcv = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        rcv.bind(('127.0.0.1', 8099))

        def recv_and_check(m):
            log.info("receiving")
            r, addr = rcv.recvfrom(4098)
            r = r.decode()
            log.info(f"got {r}")
            m = to_influx("jobs", m, tags={"FOO": "bar"}).rsplit(maxsplit=1)[0]
            self.assertTrue(r.find(m) == 0)

        def listen():
            for m in msgs:
                log.info("check...")
                recv_and_check(m)

        log.info("start")
        t = threading.Thread(target=listen)
        t.start()
        t.join(0.01)
        log.info("started")

        snd = Sender(
            queue,
            host='127.0.0.1',
            port='8099',
            tcp=False,
            tags={"FOO": "bar"}
        )
        asyncio.run(snd.send())
        asyncio.run(snd.send())

        log.info("joining...")
        t.join(0.5)
        log.info("done")

        rcv.close()





