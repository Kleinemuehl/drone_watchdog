import asyncio
import unittest
import logging
import os
import os.path
import shutil
import time

from watchdog.runner import Runner

log = logging.getLogger(__name__)


def setUpModule():
    os.mkdir("tmp")


def tearDownModule():
    shutil.rmtree("tmp")


def _create(n, name):
    log.debug("Create %s", name)
    with open(name, 'w') as f:
        f.write('foo')


class TestRunner(unittest.TestCase):
    def test_basics(self):
        log.info("Start test_basics")
        r = Runner()

        async def sleep():
            await asyncio.sleep(0.2)

        r.register(sleep, "1")
        r.register(sleep, "2")
        r.register(sleep, "3")
        t = time.time()
        r = r.run()
        self.assertTrue(time.time() - t < 0.25)
        log.debug(r)
        self.assertTrue(len(r) == 3)
        for i in r:
            self.assertTrue(i is None)

    def test_callback(self):
        log.info("Start test_callback")
        r = Runner()
        file = "tmp/foo"

        async def foo():
            await asyncio.sleep(0.1)
            self.assertFalse(os.path.exists(file))
            _create("", file)
            self.assertTrue(os.path.exists(file))
            await asyncio.sleep(0.1)

        r.register(foo, "foo", callback=lambda: os.remove(file))
        r = r.run()
        self.assertFalse(os.path.exists(file))
        log.debug(r)
        self.assertTrue(len(r) == 1)
        for i in r:
            self.assertTrue(i is None)

    def test_callback_cancel(self):
        log.info("Start test_callback_cancel")
        # log.setLevel(logging.DEBUG)
        r = Runner()
        file1 = "tmp/foo"
        file2 = "tmp/bar"

        async def foo():
            await asyncio.sleep(0.1)
            self.assertFalse(os.path.exists(file1))
            self.assertFalse(os.path.exists(file2))
            _create("", file1)
            _create("", file2)
            self.assertTrue(os.path.exists(file1))
            self.assertTrue(os.path.exists(file2))
            await asyncio.sleep(10)

        async def bar():
            await asyncio.sleep(0.3)
            r.cancel()
            # If we don't wait here, task 'bar' will have
            # ended before cancellation
            await asyncio.sleep(0.3)

        r.register(foo, "foo", callback=lambda: os.remove(file1))
        r.register(bar, "bar", callback=lambda: os.remove(file2))
        r = r.run()
        self.assertFalse(os.path.exists(file1))
        self.assertFalse(os.path.exists(file2))
        log.debug(r)
        # self.assertTrue(r is None)
        for i in r:
            self.assertTrue(i is None)














