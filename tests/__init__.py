import logging
import os
import sys


print("Running on", sys.version)


loglevel = os.environ.get('LOGLEVEL', 'WARNING')
logging.basicConfig(
    level=loglevel.upper(),
    format='%(asctime)s %(name)s@%(threadName)s %(levelname)-6s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S')

