import asyncinotify
import logging
import os
import re

import classad

from .health import set_unhealthy


class WatchDog:
    """
    Watches a file and reacts to inotify events.
    There are two ways of reacting: setting a 'handler' function or
    inheriting from this class and overriding the '_act' method.
    """

    def __init__(self, file, *, handler=None):
        from asyncinotify import Mask
        # self.file = os.path.basename(file)
        self.file = file
        self.directory = os.path.dirname(file)
        if self.directory == '':
            self.directory = '.'
        self.handler = handler
        self.inotify = asyncinotify.Inotify()
        self.dir_wd = self.inotify.add_watch(self.directory,
                                             Mask.CREATE | Mask.IGNORED)
        self.watchmask = Mask.ACCESS | Mask.MODIFY | Mask.OPEN | Mask.CREATE \
            | Mask.DELETE_SELF | Mask.ATTRIB | Mask.CLOSE | Mask.MOVE_SELF \
            | Mask.IGNORED
        self.reactmask = Mask.MODIFY
        if not hasattr(self, 'log'):
            self.log = logging.getLogger(__name__ + ".WatchDog")

    # Waits until a file is created in the directory
    async def _wait_for_file(self):
        from asyncinotify import Mask
        async for event in self.inotify:
            if event.watch != self.dir_wd:
                self.log.warning("Ignoring file inotify event")
                continue
            if Mask.IGNORED in event:
                self.log.critical("Lost dir watchdog. Exiting.")
                raise RuntimeError("Lost dir watchdog")
            elif Mask.CREATE in event:
                return

    # Registers a watchdog for the file.
    # Waits until the file is created if necessary
    # Also triggers the first action
    async def _register(self):
        while True:
            self.log.debug("Trying to register watchdog for %s", self.file)
            try:
                self.inotify.add_watch(self.file, self.watchmask)
            except FileNotFoundError:
                await self._wait_for_file()
            else:
                self.log.info("Watchdog registered")
                break
        self.__act(True)

    # Action to take. Supposed to be overridden
    def _act(self, registration=False):
        self.log.debug("Watchdog action...")

    # Groups the actions
    def __act(self, registration=False):
        self._act(registration)
        if self.handler is not None:
            self.log.debug("Calling handler...")
            self.handler(self.file)

    async def run(self):
        """Starts the watchdog event loop"""
        from asyncinotify import Mask
        await self._register()
        self.log.debug("Enter watchdog loop")
        while True:
            # Collect a batch of events
            events = [await self.inotify.get()]
            while True:
                try:
                    events.append(self.inotify.sync_get())
                except BlockingIOError:
                    break

            # Accumulate event masks
            # mask = Mask.ACCESS ^ Mask.ACCESS
            mask = Mask(0)
            for e in events:
                if e.watch == self.dir_wd:
                    continue
                self.log.debug(e)
                mask |= e.mask

            # Act on masks
            if Mask.IGNORED in mask:
                self.log.warning("Need to re-register watchdog")
                await self._register()
            elif self.reactmask & mask > 0:  # one bit is enough
                self.log.debug("Run the reaction handler")
                self.__act()


################################################################################
################################################################################


# https://htcondor.readthedocs.io/en/latest/classad-attributes/job-classad-attributes.html#JobStatus
def is_done(ad):
    return ad.get("JobStatus") == 4


# https://htcondor.readthedocs.io/en/latest/classad-attributes/job-classad-attributes.html#ExitBySignal
def was_successful(ad):
    return is_done(ad) and ad.get("ExitCode") == 0


class CondorLogWatcher(WatchDog):
    """
    Watchdog for Condor startd history files.
    Can report job stats to a queue.
    """

    def __init__(self, file, *, queue=None, **kwargs):
        self.timestamp = 0
        self.fails = 0
        self.queue = queue
        self.log = logging.getLogger(__name__ + ".LogWatcher")
        super().__init__(file, **kwargs)

    # Puts job stats into queue if defined
    def _send(self, ad):
        if self.queue is None:
            return
        self.log.debug("Fill message queue: task %s", ad.get('GlobalJobId'))
        if self.queue.qsize() == self.queue.maxsize:
            self.log.warning("Message queue is full. Dropping elements")
            self.queue.get_nowait()
        self.queue.put_nowait(ad)

    # Action to take on relevant inotify event
    def _act(self, registration=False):
        with open(self.file, mode='rt') as h:
            for ad in classad.parseAds(h):
                # Read entry
                t = ad.get('CompletionDate')
                # Filter for timestamps.
                # Can there be multiple jobs with the same CompletionDate????
                # TODO: jobs could get lost this way ?
                if t <= self.timestamp or not is_done(ad):
                    self.log.debug("  Skipping")
                    continue
                self.timestamp = t
                success = was_successful(ad)
                self.log.debug("Job %s:", ad.get('GlobalJobId'))
                self.log.debug("  owned by: %s", ad.get('Owner'))
                self.log.debug("  was successful: %s", success)
                self.log.debug("  ended: %d", t)
                if not success:
                    self.log.warning("Job failed")
                    self.fails += 1
                    if self.fails > 1:
                        set_unhealthy()
                # Fill message queue
                droneid = ad.get('RemoteHost').split('@')
                try:
                    droneid = droneid[1]
                except IndexError:
                    self.log.warning("Cannot find DroneID in %s", droneid)
                env = ad.get('environment')
                harvester_id = re.search(r'HARVESTER_ID=(\S*)', env)
                harvester_workerid = re.search(
                    r'HARVESTER_WORKER_ID=(\d*)',
                    env
                )
                apfcid = re.search(r'APFCID=(\S*)', env)
                try:
                    if harvester_id:
                        harvester_id = harvester_id.group(1)
                    if harvester_workerid:
                        harvester_workerid = harvester_workerid.group(1)
                    if apfcid:
                        apfcid = apfcid.group(1)
                except IndexError:
                    self.log.warning("Cannot parse environment")
                    # harvester_id = harvester_workerid = apfcid = ""
                msg = {"id": ad.get('GlobalJobId'),
                       "start": ad.get('JobStartDate'),
                       "end": t,
                       "droneid": droneid,
                       "harvester_id": harvester_id,
                       "harvester_workerid": harvester_workerid,
                       "apfcid": apfcid,
                       "state": "Finished" if success else "Failed",
                       "env": env}
                self._send(msg)




