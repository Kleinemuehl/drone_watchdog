import asyncio
import logging
import time


log = logging.getLogger(__name__ + ".sender")


def to_influx(measurement: str, msg: dict, *, tags: dict = {}) -> str:
    """
    Converts a dict to the Influx line protocol.
    """
    tags = ''.join([",{}={}".format(k, v) for k, v in tags.items()])
    msg = ','.join(["{}=\"{}\"".format(k, v) for k, v in msg.items()])
    t = time.time_ns()
    return "{}{} {} {}".format(measurement, tags, msg, t)


class ClientProtocol(asyncio.DatagramProtocol):
    """
    The client protocol for a UDP connection
    """
    def __init__(self, message, on_con_lost):
        self.message = message
        self.on_con_lost = on_con_lost
        self.transport = None

    def connection_made(self, transport):
        self.transport = transport
        self.transport.sendto(self.message.encode())
        log.debug('Data sent: {!r}'.format(self.message))
        self.transport.close()

    # Should not be called at all...
    def datagram_received(self, data, addr):
        log.warning("Received data:", data.decode())
        # self.transport.close()

    def error_received(self, exc):
        log.error('Error received:', exc)
        raise exc

    def connection_lost(self, exc):
        log.debug('Connection closed')
        if exc:
            raise exc
        try:
            self.on_con_lost.set_result(True)
        except asyncio.InvalidStateError:
            log.warning("Trying to end future that's already done.")


class TelegrafClient():
    """
    This class represents a connection to telegraf.
    """
    def __init__(self, host: str, port: int, *,
                 timeout=0.2, retries=10, tags: dict = {}):
        self.host = host
        self.port = port
        self.tags = tags
        self.timeout = timeout
        self.retries = retries

    # async def _sock(self, type):
    #     sock = socket.socket(type = type)
    #     sock.settimeout(self.timeout)
    #     i = 0
    #     log.debug("Creating socket...")
    #     while True:
    #         i += 1
    #         try:
    #             sock.connect((self.host, self.port))
    #         except TimeoutError:
    #             if i > self.retries: raise
    #             log.warning("Connection timeout. Retry #%i", i)
    #             sock = socket.socket(type = type)
    #             sock.settimeout(self.timeout)
    #             await asyncio.sleep(self.timeout)
    #         else:
    #             break
    #     return sock

    async def send_tcp(self, msg: str):
        # sock = await self._sock(type = socket.SOCK_STREAM)

        log.debug("Sending 1...")
        # reader, writer = await asyncio.open_connection(sock = sock)
        reader, writer = await asyncio.open_connection(self.host, self.port)
        log.debug("Sending 2...")
        writer.write(msg.encode())
        await writer.drain()
        log.debug("Closing socket...")
        writer.close()
        await writer.wait_closed()

    async def send_udp(self, msg: str):
        # sock = await self._sock(type = socket.SOCK_DGRAM)

        # icmp_sock = socket.socket(
        #     type=socket.SOCK_RAW,
        #     proto=socket.getprotobyname('icmp')
        # )
        # icmp_sock.setblocking(False)

        loop = asyncio.get_running_loop()
        on_con_lost = loop.create_future()
        transport, protocol = await loop.create_datagram_endpoint(
            lambda: ClientProtocol(msg, on_con_lost),
            # sock = sock
            remote_addr=(self.host, self.port)
        )
        try:
            await on_con_lost
        finally:
            # Shouldn't be necessary, but to be sure...
            transport.close()
        # with async_timeout.timeout(0.5):
        #     r = await loop.sock_recv(icmp_sock, 1024)
        #     header = r[20:24]
        #     type, code, checksum = struct.unpack(
        #         "bbH", header
        #     )

    async def send(self, *args, tags: dict = {}, tcp=False, **kwargs):
        _tags = self.tags.copy()
        _tags.update(tags)
        msg = to_influx(*args, tags=_tags)
        # print(msg)
        if tcp:
            await self.send_tcp(msg)
        else:
            await self.send_udp(msg)

        # sock = Client('125.0.0.3', 'aa')
        # asyncore.loop(timeout=3, count=2)

        # loop = asyncio.get_running_loop()
        # on_con_lost = loop.create_future()
        # transport, protocol = await loop.create_connection(
        #     lambda: ClientProtocol(msg, on_con_lost),
        #     self.host, self.port)
        # try:
        #     await on_con_lost
        # finally:
        #     transport.close()

        # sock = socket.socket(type = socket.SOCK_STREAM | socket.SOCK_NONBLOCK)

        # reader, writer = await asyncio.open_connection(
        #     self.host, self.port)
        # writer.write(msg)
        # await writer.drain()
        # writer.close()
        # await writer.wait_closed()


################################################################################
################################################################################

class Sender():
    """
    This class represents a connection to telegraf.
    Will fetch its metrics from the given queue
    """

    def __init__(self, queue, host, port, *, tcp=True, tags={}):
        self.queue = queue
        self.log = logging.getLogger(__name__ + ".Sender")
        self.tcp = tcp
        self.client = TelegrafClient(
            host=host,
            port=port,
            tags=tags)

    # Retrieve one item from the queue and send it
    async def send(self):
        msg = await self.queue.get()
        self.log.debug("Trying to send %r", msg)

        # aiotelegraf suppresses all errors >.<
        # await self.client.connect()
        # self.client.metric("jobs", msg, tags={ "a": "foo" })
        # await self.client.close()

        try:
            await self.client.send("jobs", msg, tags={}, tcp=self.tcp)
        except (ConnectionRefusedError, TimeoutError) as e:
            self.log.error("Cannot send metrics: %r", e)
            raise
        finally:
            self.queue.task_done()

    # Starts a loop that processes the queue forever
    async def run(self):
        while True:
            await self.send()



