import asyncio
import functools
import logging
import signal


class Runner:
    """
    Runner for async tasks. Supposed to handle exceptions and cancellation
    """

    def __init__(self):
        #if sys.version_info >= (3, 10):
        #    self.loop = asyncio.new_event_loop()
        #else:
        self.loop = asyncio.get_event_loop()
        if self.loop.is_closed():
            self.loop = asyncio.new_event_loop()
            asyncio.set_event_loop(self.loop)
        # self.loop.set_debug(True)
        self.loop.add_signal_handler(signal.SIGTERM, self._cancel)
        self.tasks = set()
        self.log = logging.getLogger(__name__ + ".Runner")

    def register(self, payload, name, optional=False, callback=None):
        """Register a task. Does not run it"""

        # Callback function that runs after a task has ended in any way
        def _done_callback(task, name):
            self.log.debug("Callback %s: started", name)
            if callback is not None:
                callback()
            try:
                task.result()
            except asyncio.CancelledError:
                self.log.info('Callback %s: CancelledError', name)
            except Exception as e:
                self.log.warning('Exception raised by task %s = %r -- %s',
                                 name, task, repr(e))
            else:
                self.log.info("Callback %s: Task ended", name)

        # Every task is wrapped into this function to handle exceptions
        async def _payload_wrapper(payload, name, optional):
            try:
                result = await payload()
            except asyncio.CancelledError as e:
                self.log.info("Interrupt in _payload_wrapper: %s - %r",
                              name, e)
                return None
            except KeyboardInterrupt:
                self.log.warning(
                    "_payload_wrapper %s caught a KeyboardInterrupt!", name)
                self.cancel()
                return None
                # raise
            except BaseException as e:
                self.log.error(
                    "Exception in _payload_wrapper: %s - %r", name, e)
                if optional:
                    self.log.error("Task %s marked optional. Continuing", name)
                else:
                    raise
                return None
            else:
                return result

        # Create and add the task
        t = self.loop.create_task(
            _payload_wrapper(payload, name, optional),
            name=name
        )
        t.add_done_callback(functools.partial(_done_callback, name=name))
        self.tasks.add(t)

    # Run all tasks until completion
    async def _run(self):
        tasks = asyncio.gather(*self.tasks)
        try:
            r = await tasks
        except asyncio.CancelledError:
            self.log.error("Unhandled exception: CancelledError")
        except BaseException as e:
            self.log.error("Unhandled exception: %r", e)
            for t in self.tasks:
                self.log.error("Trying to cancel %r", t)
                # NOTE: Cancellation is done automatically if using asyncio.run
                t.cancel()
        else:
            return r

    def run(self):
        """Start all registered tasks. Blocking"""
        asyncio.set_event_loop(self.loop)
        r = self.loop.run_until_complete(self._run())
        self.log.debug("Task group ended")
        self._close()
        return r

    def _cancel(self):
        for t in self.tasks:
            self.log.debug("Cancelling %r", t)
            t.cancel()

    def cancel(self):
        """Cancels all registered tasks"""
        self.log.info("Cancelling all tasks")
        self.loop.call_soon(self._cancel)
        # Do we need to wait here before returning?

    # Cleanup
    def _close(self):
        # if self.loop.is_closed():
        #     return
        self.loop.run_until_complete(asyncio.sleep(0))
        self.log.debug("Waiting for all tasks to finish")
        for t in filter(lambda t: not t.done() and not t.cancelled(),
                        self.tasks):
            self.log.debug("Waiting for %r", t)
            self.loop.run_until_complete(t)
            self.log.debug("... finished")
        #asyncio.set_event_loop(None)
        self.loop.run_until_complete(self.loop.shutdown_asyncgens())
        self.loop.close()
        self.log.debug("Closed event loop %r", self.loop)




