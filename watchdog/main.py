#!/bin/env python3

import asyncio
import functools
import logging
import os
import sys
import time

from watchdog.runner import Runner
from watchdog.watchdog import CondorLogWatcher
from watchdog.sender import Sender
from watchdog.health import set_unhealthy


################################################################################
################################################################################

def cvmfs_gone():
    log.warning("Cannot open CVMFS")
    set_unhealthy()


class PathChecker:
    """
    Checks repeatedly whether a path is accessible.
    Will take an action if not.
    """
    def __init__(self, path, action, interval=60):
        self.path = path
        self.action = action
        self.interval = interval
        self.max_strikes = 5
        self.max_con_strikes = 3
        self.log = logging.getLogger(__name__ + ".PathChecker")

    async def run(self):
        strikes = 0
        con_strikes = 0
        while True:
            if not os.path.exists(self.path):
                self.log.warn("%s does not exist", self.path)
                strikes += 1
                con_strikes += 1
                if strikes >= self.max_strikes \
                        or con_strikes >= self.max_con_strikes:
                    self.log.error(
                        "Too many strikes for %s. Marking unhealthy...",
                        self.path
                    )
                    self.action()
            else:
                con_strikes = 0
            await asyncio.sleep(self.interval)


################################################################################
################################################################################


async def timer(log):
    start = time.time()
    while True:
        log.info(f'Time: {time.time() - start:.2f}')
        await asyncio.sleep(10)


def main():
    # Prepare main logger
    loglevel = os.environ.get('LOGLEVEL', 'INFO')
    logging.basicConfig(
        level=loglevel.upper(),
        format='%(asctime)s %(name)s %(levelname)-6s: %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S')
    global log
    log = logging.getLogger(__name__)
    log.info("Logging set up")
    log.info("Running on: %s", sys.version)

    # Check python version
    if sys.version_info < (3, 7):
        log.error("Python version too old. Need >= 3.8")
        sys.exit(1)
    elif sys.version_info < (3, 8):
        log.warning("Python 3.7 is untested. \
            In case of problems upgrade to >= 3.8")

    # Check args
    file = ''
    if len(sys.argv) < 2:
        log.error("Filename required")
        sys.exit(1)
    else:
        file = sys.argv[1]

    # Check environment
    tardis_instance = os.environ.get('TARDIS_INSTANCE', 'unknown')

    # Register tasks
    # Changed in version 3.8:
    #   CancelledError is now a subclass of BaseException.
    # If the exception is an instance of BaseException but not Exception
    # (such as KeyboardInterrupt), it bubbles up
    # https://github.com/python/asyncio/issues/341
    queue = asyncio.Queue(10)
    snd = Sender(
        queue,
        host='d84kleinm.physik.uni-wuppertal.de',
        port=8094,
        tags={"TARDIS_INSTANCE": tardis_instance},
    )
    wd = CondorLogWatcher(file, queue=queue)
    cvmfs_checker = PathChecker(
        '/cvmfs/atlas.cern.ch/repo/ATLAS_LOCAL_ROOT_BASE',
        cvmfs_gone
    )
    r = Runner()
    r.register(functools.partial(timer, log), "timer")
    r.register(wd.run, "WatchDog")
    r.register(snd.run, "Sender", optional=True)
    r.register(cvmfs_checker.run, "CVMFS", optional=True)
    # r.register(cvmfs_checker.run, "CVMFS", optional=True,
    #     callback=lambda: time.sleep(3))

    # Start
    try:
        r.run()
    except KeyboardInterrupt:
        log.warning("KeyboardInterrupt caught")
    finally:
        r.cancel()
        r._close()
        # asyncio.run(r.wait())

    print("Reached end")



