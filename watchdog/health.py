import os
import logging

import htcondor


log = logging.getLogger(__name__)


def drain_node():
    log.warn("Trying to drain the node...")
    try:
        name = "slot1@" + htcondor.param['TardisDroneUuid']
        log.info("Drone name %s", name)
    except KeyError:
        log.error("Cannot read drone name. Cannot drain node")
    else:
        if os.system("condor_drain " + name) != 0:
            log.error("Cannot drain node")


def set_unhealthy():
    log.warning("Trying to mark node unhealthy")
    if os.system('condor_config_val -rset "NODE_IS_HEALTHY = false"') != 0:
        log.error("Cannot mark node unhealthy")
        drain_node()
    elif os.system('condor_reconfig') != 0:
        log.error("Cannot mark node unhealthy")
        drain_node()
        # raise RuntimeError
